function prandtl(d, k, r, lambda_guess, bailout) result(lambda)
  real, intent(in)    :: d, k, r, lambda_guess
  real                :: current_lambda, lambda
  integer, intent(in) :: bailout
  print *, "INFO : Prandtl"
  current_lambda = lambda_guess
  do i=0, bailout,1
     lambda = (2.0 * LOG10(r * (current_lambda ** 0.5)) - 0.8) ** (-2.0)
     if (ABS(current_lambda - lambda) .LT. 2e-9) then
        print *, "     : no. iterations = ", i
        print *, "     : lambda = ", lambda
        return
     else
        current_lambda = lambda
     end if
  end do
  print *, "     : Exhausted bailout"
  print *, "     : lambda = ", current_lambda
end function prandtl

function torricelli(delta_z, lambda, length, diameter) result(speed)
  real, intent(in)    :: delta_z, lambda, length, diameter
  real                :: speed, denominator, numerator
  print *, "INFO : Torricelli"
  numerator = 2.0 * 9.81 * delta_z
  denominator = 1.0 + (lambda * length / diameter)
  speed = (numerator / denominator) ** 0.5
  print*, "     : = ", speed, "m/s"
end function torricelli

function reynolds(u, diameter, viscosity) result(reynold)
  real, intent(in)   :: diameter, viscosity, u
  real               :: reynold
  print *, "INFO : Reynolds"
  reynold = u * diameter / viscosity
  print *, "     : = ", reynold
end function reynolds

function speed_iter(delta_z, lambda_guess, length, diameter, viscosity, k, iter_bailout) result(current_speed)
  real,    intent(in) :: delta_z, lambda_guess, length, diameter, viscosity, k
  integer, intent(in) :: iter_bailout
  real                :: current_speed, new_lambda, re, lambda
  lambda = lambda_guess
  do i=0,iter_bailout,1
     print *, "===================================="
     print *, "INFO : Speed iteration round no. ", i
     current_speed = torricelli(delta_z, lambda, length, diameter)
     re = reynolds(current_speed, diameter, viscosity)
     new_lambda = prandtl(diameter, k, re, lambda, iter_bailout)

     if (ABS(lambda - new_lambda) .LT. 2e-9) then
        return
     else
        lambda = new_lambda
        print *, "     : Not enough yet. Do it again"
     end if
  end do
  print *, "WARN : Speed iter bailout exhausted."
  print *, "     : I'll return here"
end function speed_iter

program darcy_weisbach
  real :: dz, lambda_guess, length, diameter, viscosity, k, u
  dz = 4.0
  lambda_guess = 0.02
  length = 10.0
  diameter = 0.0075
  viscosity = 10e-7
  k = 0.0015
  u = speed_iter(dz, lambda_guess, length, diameter, viscosity, k * 10e-3, 3000)
  print *, "FINAL SPEED u = ", u, "m/s"
end program darcy_weisbach
