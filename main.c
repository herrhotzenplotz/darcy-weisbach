/* Copyright 2020 Nico Sonack
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "lambda.h"


/* EXAMPLE FOR A SMOOTH PIPE REGIEME OUTFLOW
 * - big container
 * - p1 = p2
 * - levels: z1 = 5m, z2 = 1m
 * - length = 10m
 * - pipe diameter = 7.5mm
 * - roughness of the pipe = 0.0015mm
 * - kinematic viscosity of water = 1e-6 m^2/s
 */


/* Modified Torricelli Equation to account for loss in energery caused
 * by friction.
 */
static
double speed(double delta_z,
             double lambda,
             double length,
             double diameter)
{
    printf("INFO : Torricelli\n");
    double numerator = 2 * 9.81 * delta_z,
        denominator = 1.0 + (lambda * length / diameter);
    double result = sqrt( numerator / denominator );
    printf("     : = %lf m/s\n", result);
    return result;
}

static
double reynold(double u,
               double diameter,
               double viscosity)
{
    printf("INFO : Reynolds\n");
    double result = u * diameter / viscosity;
    printf("     : = %lf\n", result);
    return result;
}

static
double speed_iter(double delta_z,
                  double lambda_guess,
                  double length,
                  double diameter,
                  double viscosity,
                  double k,
                  size_t iter_bailout)
{
    double lambda = lambda_guess;
    for (size_t i = 0; i < iter_bailout; ++i) {
        printf("====================================\n");
        printf("INFO : Speed iteration round no. %u\n", i);
        double curr_speed = speed(delta_z, lambda, length, diameter);
        double Re = reynold(curr_speed, diameter, viscosity);
        double new_lambda = prandtl(diameter, k, Re, lambda, iter_bailout);
        if ( fabs(lambda - new_lambda) < LAMBDA_EPSILON ) {
            return curr_speed;
        } else {
            printf("     : Not enough yet. Do it again.\n");
            lambda = new_lambda;
            continue;
        }
    }

    printf("WARN : Speed iter bailout exhausted.\n");
    printf("     : I'll return here.\n");
    return speed(delta_z, lambda, length, diameter);
}

int main(void)
{
    double dz = 4.0,
        lambda_guess = 0.02,
        length = 10.0,
        diameter = 0.0075,
        viscosity = 10E-7,
        k = 0.0015;

    double u = speed_iter(dz, lambda_guess, length, diameter, viscosity,
                          k * 10E-3, 3000);

    printf("INFO : SOLUTION FOR u = %lf\n", u);
    return EXIT_SUCCESS;
}
