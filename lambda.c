/* Copyright 2020 Nico Sonack
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "lambda.h"

#include <stdio.h>
#include <math.h>

static inline
double lg(double d)
{
    return log(d) / log(10);
}

/* Some reference material:
 * https://en.wikipedia.org/wiki/Darcy%E2%80%93Weisbach_equation
 *
 * See especially: Darcy-friction factor
 * Note: Wikipedia calls lambda f_D for whatever reason.
 */

/* Rough pipe regieme (Prandtl-Colebrook solution)
 */
double prandtl_colebrook(double diameter,
                         double k,
                         double reynold,
                         double lambda_guess,
                         size_t bailout)
{
    printf( "INFO : Prandtl-Colebrook\n");

    double current_lambda = lambda_guess;
    for (size_t i = 0; i < bailout; ++i) {
        double new_lambda = pow( 1.74 - 2 * lg( (2 * diameter / k) + (18.7 / (reynold * sqrt(current_lambda))) ), -2.0 );
        if ( fabs(current_lambda - new_lambda) < LAMBDA_EPSILON ) {
            printf( "     : took %u iterations to get lambda = %lf\n", i + 1, new_lambda );
            return new_lambda;
        } else {
            current_lambda = new_lambda;
            continue;
        }
    }

    printf( "     : Exhausted bailout\n" );
    printf( "     : lambda = %lf\n", current_lambda );
    return current_lambda;
}

/* Smooth pipe regieme (Prandtl solution)
 */
double prandtl(double diameter,
               double k,
               double reynold,
               double lambda_guess,
               size_t bailout)
{
    (void) k;
    (void) diameter;
    printf( "INFO : Prandtl\n");

    double current_lambda = lambda_guess;
    for (size_t i = 0; i < bailout; ++i) {
        double new_lambda = pow( 2 * lg( reynold * sqrt(current_lambda) ) - 0.8, -2.0 );
        if ( fabs(current_lambda - new_lambda) < LAMBDA_EPSILON ) {
            printf( "     : took %u iterations to get lambda = %lf\n", i + 1, new_lambda );
            return new_lambda;
        } else {
            current_lambda = new_lambda;
            continue;
        }
    }

    printf( "     : Exhausted bailout\n" );
    printf( "     : lambda = %lf\n", current_lambda );
    return current_lambda;
}
