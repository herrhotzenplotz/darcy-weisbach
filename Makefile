CC=cc
F90?=gfortran
CFLAGS=-O2 -Wall -Wextra -Werror -fno-builtin -std=c99 -pedantic -pedantic-errors
LIBS=-lm
OBJS=        \
    lambda.o \
    main.o

.PHONY: all
all: Makefile darcy-weisbach-c darcy-weisbach-f90

darcy-weisbach-f90: darcy-weisbach.f90
	$(F90) -o darcy-weisbach-f90 darcy-weisbach.f90

darcy-weisbach-c: $(OBJS)
	$(CC) $(CFLAGS) $(LIBS) $(OBJS) -o $@

.c.o:
	$(CC) -c $< $(CFLAGS) -o $@

clean:
	rm -f $(OBJS) darcy-weisbach-f90 darcy-weisbach-c
