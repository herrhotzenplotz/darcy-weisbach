# Darcy-Weisbach solver

## Synopsis

Solution finder for a simple Darcy-Weisbach problem.

Implemented in pure C99. Thus it is cross-platform.
Also, I recently added a Fortran90 version.

## Quick start

```sh
$ git clone https://gitlab.com/herrhotzenplotz/darcy-weisbach
$ cd darcy-weisbach/
$ make
$ ./darcy-weisbach-c   # for the c version
$ ./darcy-weisbach-f90 # for the f90 version
```

## License

This is licensed under a 3-clause BSD license.

## Results

+ Tested on `FreeBSD triton 12.1-RELEASE-p6 FreeBSD 12.1-RELEASE-p6 #1 r363009M: Thu Jul  9 06:00:18 CEST 2020     root@triton:/usr/obj/usr/src/amd64.amd64/sys/TRITON  amd64 amd64 1201000 120100`
`

+ Result should be 1.36 m/s
+ Found is 1.368547 m/s which is (almost) perfect

## BSD-license

```raw
Copyright 2020 Nico Sonack

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

```
